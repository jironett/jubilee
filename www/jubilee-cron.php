<?php

 $conf = array(
	"dbname"	=>"jubilee",
	"dbserver"	=>"localhost",
	"dbuser"	=>"root",
	"dbpasswd"	=>"",
	"firstEmail"	=>8,
	"firstEmailDay"	=>"dnů",
	"secondEmail"	=>1,
	"secondEmailDay"=>"den"
);

$link = mysql_connect($conf['dbserver'], $conf['dbuser'], $conf['dbpasswd']);
mysql_select_db($conf['dbname']);
mysql_query("SET CHARACTER SET utf8");

$persons = queryDBdata("SELECT * FROM person");
$tmStamp = strtotime(date('Y-m-d', time()));
$firstMin = $tmStamp+60*60*24*($conf['firstEmail']-1);
$firstMax = $tmStamp+60*60*24*$conf['firstEmail'];
$secondMin = $tmStamp+60*60*24*($conf['secondEmail']-1);
$secondMax = $tmStamp+60*60*24*$conf['secondEmail'];

foreach ($persons as $key => $person){
	$birthdayArr = explode('-', $person['birthday']);
	$birthdayArr[0] = date('Y');
	$birthdayTS = strtotime(implode('-', $birthdayArr));
	if($birthdayTS > $firstMin && $birthdayTS <=$firstMax){
		$person['birthdayMessage'] = '<tr>
						<td nowrap>'.$person['name'].'</td>
						<td nowrap>'.date('j. n. Y', strtotime($person['birthday'])).'</td>
						<td>'.substr($person['note'], 0, 120).'</td>
					      </tr>';
		$birthdaysFisrt[] = $person;
	}
	if($birthdayTS > $secondMin && $birthdayTS <=$secondMax){
		$person['birthdayMessage'] = '<tr>
						<td nowrap>'.$person['name'].'</td>
						<td nowrap>'.date('j. n. Y', strtotime($person['birthday'])).'</td>
						<td>'.substr($person['note'], 0, 120).'</td>
					      </tr>';
		$birthdaysSec[] = $person;
	}
	$feastArr = explode('-', $person['feast']);
	$feastArr[0] = date('Y');
	$feastImpl = implode('-', $feastArr);
	$feastTS = strtotime($feastImpl);
	if($feastTS > $firstMin && $feastTS <= $firstMax){
		$person['feastMessage'] = '<tr>
						<td nowrap>'.$person['name'].'</td>
						<td nowrap>'.date('j. n.', strtotime($feastImpl)).'</td>
						<td>'.substr($person['note'], 0, 120).'</td>
					      </tr>';
		$feastsFirst[] = $person;
	}
	if($feastTS > $secondMin && $feastTS <= $secondMax){
		$person['feastMessage'] = '<tr>
						<td nowrap>'.$person['name'].'</td>
						<td nowrap>'.date('j. n. Y', strtotime($feastImpl)).'</td>
						<td>'.substr($person['note'], 0, 120).'</td>
					      </tr>';
		$feastsSec[] = $person;
	}
  }

$celebrations = queryDBdata("SELECT * FROM celebration ORDER BY name ASC");
foreach ($celebrations as $key => $celebration){
	$celebrationArr = explode('-', $celebration['date']);
	$celebrationArr[0] = date('Y');
	$celebrationTS = strtotime(implode('-', $celebrationArr));
	if($celebrationTS > $firstMin && $celebrationTS <= $firstMax){
		$celebration['celebrationMessage'] = '<tr>
						<td nowrap>'.$celebration['name'].'</td>
						<td nowrap>'.date('j. n. Y', strtotime($celebration['date'])).'</td>
						<td>'.substr($celebration['note'], 0, 120).'</td>
					      </tr>';
		$celebrationFisrt[] = $celebration;
	}
	if($celebrationTS > $secondMin && $celebrationTS <= $secondMax){
		$celebration['celebrationMessage'] = '<tr>
						<td nowrap>'.$celebration['name'].'</td>
						<td nowrap>'.date('j. n. Y', strtotime($celebration['date'])).'</td>
						<td>'.substr($celebration['note'], 0, 120).'</td>
					      </tr>';
		$celebrationSec[] = $celebration;
	}
  }

/* --- EMAIL TEMPLATE --- */

$head = '<!DOCTYPE html>
		<html lang="cs">
		<head>
			<meta charset="utf-8" />
			<style>
				body {font-family: Tahoma;}
				.container-mail {width: 700px; margin: 0 auto;}
				table tr td {padding: 10px; background-color: #f9f9f9;}
			</style>
		</head>
		<body>
		<div class="container-mail">
		  <h3><a href="http://jubilee.jironett.cz">Jubilee</a> - zase se něco blíží...</h3>';

$birthdayTable = '<h4>Narozeniny</h4><table><tr><th>Jméno</th><th>Narozeniny</th><th>Poznámky</th></tr>';
$feastTable = '<h4>Svátky</h4><table><tr><th>Jméno</th><th>Svátek</th><th>Poznámky</th></tr>';
$celebrationTable = '<h4>Události</h4><table><tr><th>Název</th><th>Datum</th><th>Poznámky</th></tr>';

$emailHeader  = "MIME-Version: 1.0\r\n";
$emailHeader .= "Content-type: text/html; charset=UTF-8\r\n";
$emailHeader .= "From: admin@jironett.cz\r\n";

$users = queryDBdata("SELECT * FROM user");
foreach ($users as $key => $user){

	/* First EMAIL */
	foreach($birthdaysFisrt as $key => $person){
		if($user['id'] == $person['id_user'] && $person['birthdayMessage'] != ''){
			$birthdaysRows .= $person['birthdayMessage'];
		}
	}

	foreach($feastsFirst as $key => $feast){
		if($user['id'] == $feast['id_user'] && $feast['feastMessage'] != ''){
			$feastsRows .= $feast['feastMessage'];
		}
	}

	foreach($celebrationFisrt as $key => $celebration){
		if($user['id'] == $celebration['id_user'] && $celebration['celebrationMessage'] != ''){
			$celebrationRows .= $celebration['celebrationMessage'];
		}
	}
	if($birthdaysRows != '' || $feastsRows != '' || $celebrationRows != ''){
		$birthday = ($birthdaysRows != '') ? $birthdayTable.$birthdaysRows.'</table>' : '';
		$feasts = ($feastsRows != '') ? $feastTable.$feastsRows.'</table>' : '';
		$celebrations = ($celebrationRows != '') ? $celebrationTable.$celebrationRows.'</table>' : '';

		$messageFirst = $head.'<h4>za '.$conf['firstEmail'].' '.$conf['firstEmailDay'].'</h4>'.$birthday.$feasts.$celebrations.'</div></body></html>';
		mail($user['email'],"Jubilee - zase se něco blíží... už za ".$conf['firstEmail']." ".$conf['firstEmailDay'],$messageFirst, $emailHeader);
		$birthdaysRows = '';
		$feastsRows = '';
		$celebrationRows = '';
	}

	/* Secondary EMAIL */

	foreach($birthdaysSec as $key => $person){
		if($user['id'] == $person['id_user']){
			$birthdaysSecRows .= $person['birthdayMessage'];
		}
	}

	foreach($feastsSec as $key => $feast){
		if($user['id'] == $feast['id_user']){
			$feastsSecRows .= $feast['feastMessage'];
		}
	}

	foreach($celebrationSec as $key => $celebration){
		if($user['id'] == $celebration['id_user']){
			$celebrationSecRows .= $celebration['celebrationMessage'];
		}
	}

	if($birthdaysSecRows != '' || $feastsSecRows != '' || $celebrationSecRows != ''){
		$birthdaySec = ($birthdaysSecRows != '') ? $birthdayTable.$birthdaysSecRows.'</table>' : '';
		$feastsSec = ($feastsSecRows != '') ? $feastTable.$feastsSecRows.'</table>' : '';
		$celebrationsSec = ($celebrationSecRows != '') ? $celebrationTable.$celebrationSecRows.'</table>' : '';

		$messageSec = $head.'<h4>za '.$conf['secondEmail'].' '.$conf['secondEmailDay'].'</h4>'.$birthdaySec.$feastsSec.$celebrationsSec.'</div></body></html>';
		mail($user['email'],"Jubilee - zase se něco blíží... už za ".$conf['secondEmail']." ".$conf['secondEmailDay'],$messageSec, $emailHeader);
		$birthdaysSecRows = '';
		$feastsSecRows = '';
		$celebrationSecRows = '';
	}

}

function queryDBdata ($query) {
	$result = mysql_query("$query");
	for ($i = 0; $i < @mysql_num_rows($result); $i ++) {
		$returnVar [$i] = @mysql_fetch_array ($result, MYSQL_ASSOC);
	}
	@mysql_free_result ($result);
	return ($returnVar);
}
