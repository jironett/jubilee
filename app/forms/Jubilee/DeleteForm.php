<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */
namespace Jubilee\Form\Jubilee;

use \Nette\Application\UI;

class DeleteForm extends \Nette\Object
{	
	/** @var \Jubilee\Model\Managers\PersonManager */
	private $personManager;
	
	/** @var \Jubilee\Model\Managers\CelebrationManager */
	private $celebrationManager;	
	
	function __construct(\Jubilee\Model\Managers\PersonManager $personManager, \Jubilee\Model\Managers\CelebrationManager $celebrationManager) {
		$this->personManager = $personManager;
		$this->celebrationManager = $celebrationManager;
	}

	public function create($data) {
		$form = new UI\Form;		
		$container = $form->addContainer('checkbox');
        //      $firstItem = array_shift($data);
        //      $fluentRule = $container->addCheckbox((string) $firstItem, '0');
                foreach ($data as $key => $item){
			$formItem = $container->addCheckbox((string) $item, $key);
	//		$fluentRule = $fluentRule->addConditionOn($formItem, \Nette\Forms\Form::EQUAL, FALSE);
                }
        //      $fluentRule->addRule(\Nette\Forms\Form::FILLED, ' '); 
		$form->addSubmit('submit', 'Smazat');
		$form->onSuccess[] = $this->performDelete;
		return $form;
	}  
	
	public function performDelete($form){
		$values = $form->getValues();
		$presenter = $form->getPresenter();
		$ids = array();
		foreach($values['checkbox'] as $item => $value){
			if($value === true){
				$ids[] = $item;
			}		
		}
		switch($presenter->action){
			case "person" : ($this->personManager->deletePersons($ids)) ? $result = true : $result = false; 
					$redirect = "Jubilee:person"; break;
			case "celebration" : ($this->celebrationManager->deleteCelebration($ids)) ? $result = true : $result = false; 
					$redirect = "Jubilee:celebration"; break;
		}
		$message = "Smazáno ;-)";
		$type = "success";
		if($result === false){
			$message = "Nebyla vybraná žádná položka.";
			$type = "info";
		}
		$presenter->flashMessage($message, $type);
		$presenter->redirect($redirect);		
	}

}



