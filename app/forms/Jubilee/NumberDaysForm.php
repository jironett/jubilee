<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */
namespace Jubilee\Form\Jubilee;

use \Nette\Application\UI;

class NumberDaysForm extends \Nette\Object
{	

	public function create() {
		$form = new UI\Form;
		$form->addText("numberDays");
		$form->addSubmit("submit", "Počet zobrazovaných dní");
		$form->onSuccess[] = $this->numberDays;
		return $form;
	}
	
	public function numberDays($form){
		$presenter = $form->getPresenter();
		$presenter->redirect('Jubilee:default', $form->values->numberDays);
	}
}



