<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */
namespace Jubilee\Form\Jubilee;

use \Nette\Application\UI;
use \Jubilee\Model\Entities\Person;

class PersonForm extends \Nette\Object
{
	/** @var \Jubilee\Model\Managers\PersonManager */
	private $personManager;

	function __construct(\Jubilee\Model\Managers\PersonManager $personManager) {
		$this->personManager = $personManager;
	}

	public function create($idPerson = null) {
		$form = new UI\Form;
		$person = new Person;
		$birthday = null;
		$feast = null;
		if($idPerson){
			$person = $this->personManager->getPerson($idPerson);
			$person->__toArray();
			$birthday = ($person->birthday->format('j.n.Y') != '30.11.-0001') ? $person->birthday->format('j.n.Y') : '';
			$feast = ($person->feast->format('j.n.Y') != '30.11.-0001') ? $person->feast->format('j.n.') : '';
		}
		$form->addText('name', 'Jméno osoby: ')
			->setValue($person->name)
			->setRequired('Osoba musí mít nějaké jméno!');
		$form->addText('email', 'Email: ')
			->setValue($person->email)
			->addCondition(UI\Form::FILLED)
				->addRule(UI\Form::EMAIL, 'Zadej platnou emailovou adresu!');
		$form->addText('birthday', 'Narozeniny: ')
			->setValue($birthday)
			->addCondition(UI\Form::FILLED)
				->addRule(UI\Form::PATTERN, 'Datum je špatně zadán nebo neexistuje!', '^([1-9]|19|[12][0-8]|29(?=\.([^2]|2\.(([02468][048]|[13579][26])00|[0-9]{2}(0[48]|[2468][048]|[13579][26]))))|30(?=\.[^2])|31(?=\.([13578][02]?\.)))\.([1-9]|1[012])\.[0-9]{4}$');
		$form->addText('feast', 'Svátek: ')
			->setValue($feast)
			->addCondition(UI\Form::FILLED)
				->addRule(UI\Form::PATTERN, 'Datum svátku je špatně zadáno!', '^([1-9]|[12][0-9]|30(?=\.[^2])|31(?=\.([13578][02]?\.)))\.([1-9]|1[012])\.$');
		$form->addText('phone', 'Telefon: ')
			->setValue($person->phone)
			->setAttribute('placeholder', 'Telefon')
			->setAttribute('class', 'input-medium')
			->addCondition(UI\Form::FILLED)
				->addRule(UI\Form::PATTERN, 'Telefonní číslo zadejte ve správném tvaru', '^[+(]{0,2}[0-9 ().-]{9,}');
		$form->addTextArea('note', 'Poznámky: ')
			->setValue($person->note);
		$form->addHidden("id", $idPerson);
		$form->addSubmit('submit', 'Potvrdit');
                $form->addProtection('Vypršel časový limit, odešlete formulář znovu.');
		$form->onSuccess[] = $this->doPerson;
		return $form;
	}

	public function doPerson($form) {
		$values = $form->getValues();
		$presenter = $form->getPresenter();
		$personData = array ("birthday" => $values->birthday,
				     "feast" => $values->feast,
				     "email" => $values->email,
				     "phone" => $values->phone,
				     "note" => $values->note);
		$idPerson = null;
		if($values->id !== ""){
			$idPerson = $values->id;
		}
		$this->personManager->doPerson($presenter->getUser()->getId(), $values->name, $personData, $idPerson);
		$presenter->flashMessage("Osoba byla úspěšně uložena ;-)", "success");
		$presenter->redirect('Jubilee:person');
	}

}



