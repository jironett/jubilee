<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */
namespace Jubilee\Form\Jubilee;

use \Nette\Application\UI;
use \Jubilee\Model\Entities\Celebration;

class CelebrationForm extends \Nette\Object
{	
	/** @var \Jubilee\Model\Managers\CelebrationManager */
	private $celebrationManager;
	
	function __construct(\Jubilee\Model\Managers\CelebrationManager $celebrationManager) {
		$this->celebrationManager = $celebrationManager;
	}

	public function create($idCelebration = null) {
		$form = new UI\Form;
		$celebration = new Celebration;
		$date = null;
		if($idCelebration){
			$celebration = $this->celebrationManager->getCelebration($idCelebration);
			$celebration->__toArray();
			$date = $celebration->date->format('j.n.Y');
		}
		$form->addText('name', 'Název: ')
			->setValue($celebration->name)
			->setRequired('Událost musí mít nějaké jméno!'); 
		$form->addText('date', 'Datum: ')
			->setValue($date)
			->addCondition(UI\Form::FILLED)
				->addRule(UI\Form::PATTERN, 'Datum je špatně zadán nebo neexistuje!', '^([1-9]|19|[12][0-8]|29(?=\.([^2]|2\.(([02468][048]|[13579][26])00|[0-9]{2}(0[48]|[2468][048]|[13579][26]))))|30(?=\.[^2])|31(?=\.([13578][02]?\.)))\.([1-9]|1[012])\.[0-9]{4}$');
		$form->addTextArea('note', 'Poznámky: ')
			->setValue($celebration->note);
		$form->addHidden("id", $idCelebration);
		$form->addSubmit('submit', 'Potvrdit');
                $form->addProtection('Vypršel časový limit, odešlete formulář znovu.');
		$form->onSuccess[] = $this->doCelebration; 
		return $form;
	}  
	
	public function doCelebration($form) { 
		$values = $form->getValues();
		$presenter = $form->getPresenter();
		$celebrationData = array ("date" => $values->date,
					  "note" => $values->note);
		$idCelebration = null;
		if($values->id !== ""){
			$idCelebration = $values->id;
		}
		$this->celebrationManager->doCelebration($presenter->getUser()->getId(), $values->name, $celebrationData, $idCelebration);
		$presenter->flashMessage("Událost byla úspěšně uložena ;-)", "success");
		$presenter->redirect('Jubilee:celebration');
	}

}



