<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */
namespace Jubilee\Form\Jubilee;

use \Nette\Application\UI;

class ChangeEmailForm extends \Nette\Object
{
	/** @var \Jubilee\Model\Managers\UserManager */
	private $userManager;
	
	function __construct(\Jubilee\Model\Managers\UserManager $userManager) {
		$this->userManager = $userManager;
	}

	public function create() {
		$form = new UI\Form;
		$form->addPassword('password', 'Heslo: ')
			->setRequired('Zadejte své heslo!');	
		$form->addText('email', 'Nová emailová adresa: ')
			->setRequired('Zadejte prosím email!')
			->addRule($form::EMAIL, 'Zadejte novou platnou emailovou adresu!');
                
		$form->addSubmit('submitEmail', 'Potvrdit');                
		$form->onSuccess[] = $this->changeEmail;
		return $form;
	}  
	
	public function changeEmail($form) { 		
		$values = $form->getValues(); 
		$presenter = $form->getPresenter();
		
		if($presenter->isAjax()){
			$presenter->flashMessage('ddad');
			$presenter->redrawControl('flashes');
		}
		else {
		if($this->userManager->emailExists($values->email)){
				$presenter->flashMessage("Tento email je již používán.", "error");
				$presenter->redirect('Jubilee:');	
		}
		if($this->userManager->changeEmail($presenter->getUser()->identity->data['name'], $values->email, $values->password)){					
				$presenter->flashMessage("Váš email byl úspěšně změněn ;-)", "success");
				$presenter->redirect('Jubilee:'); 			
		}
		$presenter->flashMessage("Heslo bylo špatně zadáno", "error");
		$presenter->redirect('User:'); 
		}
	}	
		
}