<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */
namespace Jubilee\Form\Jubilee;

use \Nette\Application\UI;

class DeleteUserForm extends \Nette\Object
{
	/** @var \Jubilee\Model\Managers\UserManager */
	private $userManager;
	
	function __construct(\Jubilee\Model\Managers\UserManager $userManager) {
		$this->userManager = $userManager;
	}

	public function create() {
		$form = new UI\Form;
		$form->addPassword('password', 'Heslo: ')
			->setRequired('Zadejte své heslo!');
		$form->addSubmit('submitDel', 'Smazat');                
		$form->onSuccess[] = $this->deleteUser;
		return $form;
	}  
	
	public function deleteUser($form) {
		$values = $form->getValues();
		$presenter = $form->getPresenter();		
		if($this->userManager->deleteUser($presenter->getUser()->identity->data['name'], $values->password)){
			$presenter->getUser()->logout();
			$presenter->flashMessage("Váš účet byl smazán.");
			$presenter->redirect('Entrancepage:');  			
		}					
		$presenter->flashMessage("Heslo bylo špatně zadáno", "error");
		$presenter->redirect('User:');  
	}
		
}