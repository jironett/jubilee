<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */
namespace Jubilee\Form;

use \Nette\Application\UI;

class PasswordForm extends \Nette\Object
{
	/** @var \Jubilee\Model\Managers\UserManager */
	private $userManager;
	
	function __construct(\Jubilee\Model\Managers\UserManager $userManager) {
		$this->userManager = $userManager;
	}

	/**
	 * @param string $name
	 * @param boolean $forgotten forgotten PW = true (default), change PW = false 
	 * @return \Nette\Application\UI\Form
	 */
	public function create($name, $forgotten = true) {
		$form = new UI\Form;
		$form->addPassword('password', 'Heslo: ')
			->setRequired('Zadejte své heslo!')
		//	->addRule($form::PATTERN, 'Heslo musí obsahovat číslici', '.*[0-9].*')
			->addRule($form::MIN_LENGTH, 'Heslo musí mít nejméně 8 znaků!', 8);

		$form->addPassword('passwordVerify', 'Heslo pro kontrolu:')
			->setRequired('Zadejte heslo pro kontrolu!')
			->addRule($form::EQUAL, 'Hesla se neshodují!', $form['password']); 
                
		$form->addSubmit('submit', 'Potvrdit');                
                $form->addProtection('Vypršel časový limit, odešlete formulář znovu.');
		$form->addHidden("name", $name);
		if($forgotten){
			$form->onSuccess[] = $this->saveNewPassword;
		} else {
			$form->onSuccess[] = $this->changePassword;
		}
		return $form;
	}  
	
	public function saveNewPassword($form) {
		$values = $form->getValues();
		$presenter = $form->getPresenter();			
		$this->userManager->savePassword($values->name, $values->password);					
		$presenter->flashMessage("Změna hesla proběhla úspěšně ;-)", "success");
		$presenter->redirect('Entrancepage:');  
	}
	
	public function changePassword($form) { 
		$values = $form->getValues(); 
		$presenter = $form->getPresenter();
		if($this->userManager->changePassword($values->name, $values->oldPassword, $values->password)){		
			$presenter->flashMessage("Heslo bylo úspěšně změněno ;-)", "success");
			$presenter->redirect("Jubilee:");
		} else {
			$presenter->flashMessage("Původní heslo bylo špatně zadáno!", "error");
			$presenter->redirect("User:");			
		}
	}	
}