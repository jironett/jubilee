<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */
namespace Jubilee\Form\Entrancepage;

use \Nette\Application\UI;

class SignInForm extends \Nette\Object
{
	/** @var \Nette\Security\User */
	private $user;
	
	function __construct(\Nette\Security\User $user){
		$this->user = $user;
	}
	
	public function create() {
		$form = new UI\Form;
		$form->addText('username', 'Uživatelské jméno:')
			->setRequired('Není vyplněné uživatelské jméno!');		
		$form->addPassword('password', 'Heslo:')
			->setRequired('Prosím zadej heslo.');
		$form->addCheckbox('remember', 'Trvalé přihlášení');

		$form->addSubmit('submit', 'Přihlásit se');
		$form->onSuccess[] = $this->signInFormSubmitted;
		return $form;
	}
	
	public function signInFormSubmitted($form) {
		$values = $form->getValues();
		$presenter = $form->getPresenter();
		try {
			if ($values->remember) {
				$this->user->setExpiration('+ 14 days', FALSE);
			} else {
				$this->user->setExpiration('+ 30 minutes', TRUE);
			}
			$presenter->user->login($values->username, $values->password);
			$presenter->redirect('Jubilee:');
						
		} catch (\Nette\Security\AuthenticationException $e) {
   			$presenter->flashMessage($e->getMessage(), "error");
			$presenter->redirect('Entrancepage:');
		}
	}	
}