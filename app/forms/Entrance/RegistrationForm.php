<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */
namespace Jubilee\Form\Entrancepage;

use \Nette\Application\UI;

class RegistrationForm extends \Nette\Object
{
	/** @var \Jironett\Components\JreCaptchaControl */
	private $JreCaptcha;
	
	/** @var \Jubilee\Model\Managers\UserManager */
	private $userManager;
	
	function __construct(\Jironett\Components\JreCaptchaControl $JreCaptcha, \Jubilee\Model\Managers\UserManager $userManager) {
		$this->JreCaptcha = $JreCaptcha;
		$this->userManager = $userManager;
	}

	public function create() {
		$form = new UI\Form;
		$form->addText('name', 'Uživatelské jméno: ')
			->setRequired('Zadej prosím své uživatelské jméno!')
			->addRule($form::MIN_LENGTH, 'Uživatelské jméno je příliš krátké!', 3); 
			
		$form->addText('email', 'Emailová adresa: ')
			->setRequired('Zadej prosím svůj email!')
			->addRule($form::EMAIL, 'Zadej platnou emailovou adresu!');		
		
		$form->addPassword('password', 'Heslo: ')
			->setRequired('Zadej prosím své heslo!')
		//	->addRule($form::PATTERN, 'Heslo musí obsahovat číslici', '.*[0-9].*')
			->addRule($form::MIN_LENGTH, 'Heslo musí mít nejméně 8 znaků!', 8);

		$form->addPassword('passwordVerify', 'Heslo pro kontrolu:')
			->setRequired('Zadej heslo pro kontrolu!')
			->addRule($form::EQUAL, 'Hesla se neshodují!', $form['password']); 
                
		$form->addSubmit('submit', 'Potvrdit');                
                $form->addProtection('Vypršel časový limit, odešlete formulář znovu.');
		$form->onSuccess[] = $this->registration;
		return $form;
	}  
	
	public function registration($form) {
		$values = $form->getValues();
		$presenter = $form->getPresenter();			
		$verify = $this->JreCaptcha->validity();
                if ($verify['is_valid']) { 
			$result = $this->userManager->addNewUser($values);
			if ($result === true){	
				$presenter->flashMessage('Registrace proběhla úspěšně. Byl Vám zaslán aktivační email. ;-)', 'success');
				$presenter->redirect('Entrancepage:');  				
			} else {
				$presenter->flashMessage('Zadaný údaj '.$result.' je již používán.', 'error');
				$presenter->redirect('Entrancepage:registration');  										
			}							
		} else {
			$presenter->flashMessage('Zadal jste špatně ověřovací kód.', 'error');
			$presenter->redirect('Entrancepage:registration');  
			} 
	}
}