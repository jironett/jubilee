<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */
namespace Jubilee\Form\Entrancepage;

use \Nette\Application\UI;

class NewPassForm extends \Nette\Object
{
	/** @var \Jironett\Components\JreCaptchaControl */
	private $JreCaptcha;
	
	/** @var \Jubilee\Model\Managers\UserManager */
	private $userManager;
	
	function __construct(\Jironett\Components\JreCaptchaControl $JreCaptcha, \Jubilee\Model\Managers\UserManager $userManager) {
		$this->JreCaptcha = $JreCaptcha;
		$this->userManager = $userManager;
	}

	public function create() {
		$form = new UI\Form;
		$form->addText('name', 'Uživatelské jméno: ')
			->setRequired('Zadejte své uživatelské jméno!')
			->addRule($form::MIN_LENGTH, 'Uživatelské jméno je příliš krátké!', 3); 
			 
		$form->addSubmit('submit', 'Potvrdit');            
                $form->addProtection('Vypršel časový limit, odešlete formulář znovu.');
		$form->onSuccess[] = $this->sendEmailNewPass;
		return $form;
	}  
	
	public function sendEmailNewPass($form) {
		$values = $form->getValues();
		$presenter = $form->getPresenter();		                  
		$verify = $this->JreCaptcha->validity();       
		if ($verify['is_valid']) { 
			if ($this->userManager->sendEmailNewPass($values->name)){
				$presenter->flashMessage("Byl Vám zaslán email s možností obnovení hesla :-)", "success");
				$presenter->redirect('Entrancepage:'); 	      
			} else {
				$presenter->flashMessage("Neexistující nebo neaktivovaný uživatel!", "error");
				$presenter->redirect('Entrancepage:newPass'); 									
			}
		} else {
			$presenter->flashMessage("Zadal jste špatně ověřovací kod!", "error");
			$presenter->redirect('Entrancepage:newPass'); 						
		}                                      
	}	
}