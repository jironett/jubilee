<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */
namespace Jubilee\Model;

use \Nette\Security;

class Authenticator extends \Nette\Object implements Security\IAuthenticator {

	const
		COLUMN_ID = 'id',
		COLUMN_NAME = 'name',
		COLUMN_PASSWORD_HASH = 'password',
		COLUMN_ROLE = 'role';

	private $users;

	public function __construct(Repositories\UserRepository $users) {
		$this->users = $users;
	}

	/**
	 * Performs an authentication.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials) {
		list($username, $password) = $credentials;
		$row = $this->users->getUserByParams(array(self::COLUMN_NAME => $username));
		if (!$row) {
			throw new Security\AuthenticationException('Neexistující uživatel nebo chybné heslo!', self::IDENTITY_NOT_FOUND);
		} elseif (!Security\Passwords::verify($password, $row->password)) {
			throw new Security\AuthenticationException('Neexistující uživatel nebo chybné heslo!', self::INVALID_CREDENTIAL);
		}
		if ($row->active == false){
			throw new Security\AuthenticationException('Uživatel nebyl aktivován.', self::INVALID_CREDENTIAL);
		}
		$arr = $row->__toArray();
		unset($arr[self::COLUMN_PASSWORD_HASH]);
		return new Security\Identity($arr[self::COLUMN_ID], null, $arr);
	}

}
