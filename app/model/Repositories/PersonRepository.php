<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */

namespace Jubilee\Model\Repositories;

use \Jubilee\Model\Entities;

class PersonRepository extends \Nette\Object
{
	/** @var \Kdyby\Doctrine\EntityDao */
	private $dao;
 
        public function __construct(\Kdyby\Doctrine\EntityDao $dao)
        {
                $this->dao = $dao;             
        }

	/**
	 * @param \Jubilee\Model\Entities\Person $entity
	 */
	public function save($entity)
	{
		$this->dao->save($entity);
	}

	/**
	 * @param int $id
	 * @return \Jubilee\Model\Entities\Person
	 */	
	public function getPerson($id){
		return $this->dao->find($id);
	}
	
	public function getPersonUser($idUser, $idPerson){
		return $this->dao->findBy(array("id_user" => $idUser, "id" => $idPerson));
	}
	
	public function getAll($id_user)
	{
		return $this->dao->findBy(array("id_user" => $id_user));
	}
	
	public function delete($entity){
		return $this->dao->delete($entity);
	}
	
	public function numPersons($arr){
		return $this->dao->countBy($arr);
	}		

	
	
}