<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */

namespace Jubilee\Model\Repositories;

use \Jubilee\Model\Entities;

class CelebrationRepository extends \Nette\Object
{
	/** @var \Kdyby\Doctrine\EntityDao */
	private $dao;
 
        public function __construct(\Kdyby\Doctrine\EntityDao $dao)
        {
                $this->dao = $dao;             
        }

	/**
	 * @param \Jubilee\Model\Entities\Celebration $entity
	 */
	public function save($entity)
	{
		$this->dao->save($entity);
	}

	/**
	 * @param int $id
	 * @return \Jubilee\Model\Entities\Celebration
	 */	
	public function getCelebration($id){
		return $this->dao->find($id);
	}
	
	public function getCelebrationUser($idUser, $idCelebration){
		return $this->dao->findBy(array("id_user" => $idUser, "id" => $idCelebration));
	}
	
	public function getAll($id_user)
	{
		return $this->dao->findBy(array("id_user" => $id_user));
	}
	
	public function delete($entity){
		return $this->dao->delete($entity);
	}
	
	public function numCelebrations($arr){
		return $this->dao->countBy($arr);
	}	

	
	
}