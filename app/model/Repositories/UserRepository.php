<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */

namespace Jubilee\Model\Repositories;
use \Jubilee\Model\Entities,
    \Nette\Security;

class UserRepository extends \Nette\Object
{
        /** @var \Kdyby\Doctrine\EntityDao */
	private $dao;
 
        public function __construct(\Kdyby\Doctrine\EntityDao $dao)
        {
                $this->dao = $dao;             
        }
	
	/**
	 * @param int $id
	 * @return object|null
	 */
 	public function getUserById($id)
	{
		return $this->dao->find($id);
	}
	
	/**
	 * @param array $params
	 * @return object|null
	 */
 	public function getUserByParams(array $params)
	{				
		return $this->dao->findOneBy($params);		
	}
	
	/** 
	 * @param \Jubilee\Model\Entities\User $entity
	 * @return array
	 * @throws InvalidArgumentException
	 */
	public function save($entity){
		return $this->dao->save($entity);
	}
	
	public function delete($entity){
		return $this->dao->delete($entity);
	}

	/**
	 * @return array
	 */
	public function getAllUsers()
	{
		return $this->dao->findAll();
	}
	
}