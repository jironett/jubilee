<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */

namespace Jubilee\Model\Managers;

use \Jubilee\Model\Entities;
use \Jironett\Utils\Arrays;
use \Jironett\Utils\Date;

class CelebrationManager extends \Nette\Object
{

	/** @var \Jubilee\Model\Repositories\CelebrationRepository */
	private $celebrationRepository;

        public function __construct(\Jubilee\Model\Repositories\CelebrationRepository $celebrationRepository)
        {
                $this->celebrationRepository = $celebrationRepository;
        }

	/**
	 * @param int $idUser
	 * @param string $celebrationName
	 * @param array $celebrationData
	 * @param int $idCelebration - if set => update celebration
	 * @throws \Nette\InvalidArgumentException
	 */
	public function doCelebration($idUser, $celebrationName, $celebrationData, $idCelebration = null)
	{
		if(!is_int($idUser)){
			throw new \Nette\InvalidArgumentException("ID user must be set correctly (as int) !".__METHOD__);
		}
		if($celebrationName == ""){
			throw new \Nette\InvalidArgumentException("Name celebration must be set!".__METHOD__);
		}
		if($idCelebration == null){
			$entity = new Entities\Celebration;
		} else {
			$entity = $this->celebrationRepository->getCelebration($idCelebration);
		}
		$entity->fill(array("id_user" => $idUser,
				    "name" => $celebrationName,
				    "date" => Date::stringToDate($celebrationData['date'], "."),
				    "note" => $celebrationData['note']));
		$this->celebrationRepository->save($entity);
	}

	/**
	 * @return array
	 */
	public function getAllCelebrations($id_user){
		$records = $this->celebrationRepository->getAll($id_user);
		if(!$records){
			return array();
		}
		foreach($records as $entity){
			$date = $entity->date->format('j.n.Y');
			if ($date == '30.11.-0001'){
				$date = "";
			}
			$out[] = array("id" => $entity->id,
					"id_user" => $entity->id_user,
					"name" => $entity->name,
					"date" => $date,
					"note" => $entity->note);
		}
		$out = Arrays::sortBySubArray($out, "name");
		return $out;
	}

	public function deleteCelebration($idsCelebrations){
		if($idsCelebrations == null){
			return false;
		}
		foreach($idsCelebrations as $id){
			$entity = $this->getCelebration($id);
			$this->celebrationRepository->delete($entity);
		}
		return true;
	}

	public function getCelebration($id){
		return $this->celebrationRepository->getCelebration($id);
	}

	public function verifyIdCelebration($idUser, $idCelebration){
		if($this->celebrationRepository->getCelebrationUser($idUser, $idCelebration) == null){
			return false;
		}
		return true;
	}

	public function numCelebrations($idUser){
		return $this->celebrationRepository->numCelebrations(array("id_user" => $idUser));
	}

	public function dashboardData($idUser, $numDays = null){
		if($numDays){
			settype($numDays, 'int');
			if(!($numDays > 0 && $numDays <= 365)){
				$numDays = 60;
			}
		} else {
			$numDays = 60;
		}
		$data = $this->getAllCelebrations($idUser);
		$keysDates = array("date");
		$types = array("date" => "Událost");
		$dataWithDays = Date::addNumberDays($data, $keysDates);
		$out = array();
		foreach($keysDates as $keyDate){
			foreach($dataWithDays as $key => $record){
				if(array_key_exists($keyDate."NumDays", $record)){
					if($record[$keyDate."NumDays"] <= $numDays){
						$record["type"] = $types[$keyDate];
						$record["actionDate"] = $record['date'];
						$record["days"] = $record[$keyDate."NumDays"];
						$record["link"] = "Jubilee:update-celebration";
						foreach($keysDates as $keyDateUn){
							unset($record[$keyDateUn."NumDays"]);
						}
						$out[] = $record;
					}
				}
			}
		}
		return $out;
	}
}