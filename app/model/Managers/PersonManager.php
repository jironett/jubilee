<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */

namespace Jubilee\Model\Managers;

use \Jubilee\Model\Entities;
use \Jironett\Utils\Arrays;
use \Jironett\Utils\Date;

class PersonManager extends \Nette\Object
{

	/** @var \Jubilee\Model\Repositories\PersonRepository */
	private $personRepository;

        public function __construct(\Jubilee\Model\Repositories\PersonRepository $personRepository)
        {
                $this->personRepository = $personRepository;
        }

	/**
	 * @param int $idUser
	 * @param string $personName
	 * @param array $personData
	 * @param int $idPerson - if set => update person
	 * @throws \Nette\InvalidArgumentException
	 */
	public function doPerson($idUser, $personName, $personData, $idPerson = null)
	{
		if(!is_int($idUser)){
			throw new \Nette\InvalidArgumentException("ID user must be set correctly (as int) !".__METHOD__);
		}
		if($personName == ""){
			throw new \Nette\InvalidArgumentException("Name person must be set!".__METHOD__);
		}
		if($idPerson == null){
			$entity = new Entities\Person;
		} else {
			$entity = $this->personRepository->getPerson($idPerson);
		}
		$entity->fill(array("id_user" => $idUser,
				    "name" => $personName,
				    "birthday" => Date::stringToDate($personData['birthday'], "."),
				    "feast" => Date::stringToDate($personData['feast'], "."),
				    "email" => $personData['email'],
				    "phone" => $personData['phone'],
				    "note" => $personData['note']));
		$this->personRepository->save($entity);
	}

	/**
	 * @return array
	 */
	public function getAllPersons($id_user){
		$records = $this->personRepository->getAll($id_user);
		if(!$records){
			return array();
		}
		foreach($records as $entity){
			$birthday = $entity->birthday->format('j.n.Y');
			if ($birthday == '30.11.-0001'){
				$birthday = "";
			}
			$feast = $entity->feast->format('j.n.');
			if ($entity->feast->format('j.n.Y') == '30.11.-0001'){
				$feast = "";
			}
			$out[] = array("id" => $entity->id,
					"id_user" => $entity->id_user,
					"name" => $entity->name,
					"birthday" => $birthday,
					"feast" => $feast,
					"email" => $entity->email,
					"phone" => $entity->phone,
					"note" => $entity->note);
		}
		$out = Arrays::sortBySubArray($out, "name");
		return $out;
	}

	public function deletePersons($idsPersons){
		if($idsPersons == null){
			return false;
		}
		foreach($idsPersons as $id){
			$entity = $this->getPerson($id);
			$this->personRepository->delete($entity);
		}
		return true;
	}

	public function getPerson($id){
		return $this->personRepository->getPerson($id);
	}

	public function verifyIdPerson($idUser, $idPerson){
		if($this->personRepository->getPersonUser($idUser, $idPerson) == null){
			return false;
		}
		return true;
	}

	public function numPersons($idUser){
		return $this->personRepository->numPersons(array("id_user" => $idUser));
	}

	public function dashboardData($idUser, $numDays = null){
		if($numDays){
			settype($numDays, 'int');
			if(!($numDays > 0 && $numDays <= 365)){
				$numDays = 60;
			}
		} else {
			$numDays = 60;
		}
		$data = $this->getAllPersons($idUser);
		$keysDates = array("birthday", "feast");
		$types = array("birthday" => "Narozeniny", "feast" => "Svátek");
		$dataWithDays = Date::addNumberDays($data, $keysDates);
		$out = array();
		foreach($keysDates as $keyDate){
			foreach($dataWithDays as $key => $record){
				if(key_exists($keyDate."NumDays", $record)){
					if($record[$keyDate."NumDays"] <= $numDays){
						$type = $types[$keyDate];
						$record["actionDate"] = $record['feast'];
						if($keyDate === "birthday"){
							$type = $this->typeBirthday($record['birthday']);
							$record["actionDate"] = $record['birthday'];
						}
						$record["type"] = $type;
						$record["days"] = $record[$keyDate."NumDays"];
						$record["link"] = "Jubilee:update-person";
						foreach($keysDates as $keyDateUn){
							unset($record[$keyDateUn."NumDays"]);
						}
						$out[] = $record;
					}
				}
			}
		}
		return $out;
	}

	public function typeBirthday($date){
		$arrDate = explode(".", $date);
		$remaining = Date::numberDays($date);
		if($remaining['thisYear'] == true){
			$num = date('Y') - $arrDate[2];
		} else {
			$num = date('Y') - $arrDate[2] + 1;
		}
		return "Narozeniny ".$num." let";
	}

}