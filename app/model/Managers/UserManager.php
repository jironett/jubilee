<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */

namespace Jubilee\Model\Managers;

use \Jubilee\Model\Entities;
use \Nette\Security;
use \Nette\Mail;

class UserManager extends \Nette\Object
{
	const EMAIL_FROM = "Jubilee <admin@jironett.cz>";

	/** @var \Jubilee\Model\Repositories\UserRepository */
	private $userRepository;

        public function __construct(\Jubilee\Model\Repositories\UserRepository $userRepository)
        {
                $this->userRepository = $userRepository;
        }

	/**
	 * Returns boolean true or string with duplicate name or email if exists in db
	 * @param array $userData
	 * @return boolean|string
	 */
	public function addNewUser($userData)
	{
		if ($this->userRepository->getUserByParams(array("name" => $userData["name"])) !== NULL){
			return "uživatelské jméno: ".$userData["name"];
		}
		if ($this->emailExists($userData["email"])){
			return "email: ".$userData["email"];
		}
		$entity = new Entities\User;
		$token = $this->token();
		$entity->fill(array("name" => $userData["name"],
				"password" => $this->hashPW($userData["password"]),
				"email" => $userData["email"],
				"last_login" => new \DateTime('0000-00-00 00:00:00'),
				"date_registration" => new \DateTime("now"),
				"active" => 0,
				"token" => md5($token["token"]),
				"token_validity" => $token["validity"]));
		$this->userRepository->save($entity);
		$mail = new Mail\Message;
                $mail->setFrom(self::EMAIL_FROM)
                     ->addTo($userData["email"])
	             ->setSubject('Aktivace účtu - Jubilee')
		     ->setHTMLBody('<p>Zdravíčko, <br />tak právě jste se úspěšně registroval do aplikace Jubilee.<br />
				Vaše uživatelské jméno je <strong>'.$userData["name"].'</strong>.<br /><br />
				Aktivační link Vašeho účtu: <br />
				<a href="http:/jubilee.jironett.cz/vstup/activation/'.$userData["name"].'/'.$token["token"].'">http:/jubilee.jironett.cz/vstup/activation/'.$userData["name"].'/'.$token["token"].'</a>
				<br /> Platnost odkazu vyprší za 24 hodin.<br />
				<br /><br />S pozdravem<br />Jironett.cz ;-)</p>');
                $mailer = new Mail\SendmailMailer;
		$mailer->send($mail);
		return true;
	}

	/**
	 * False if user is not active or doesn't exist
	 * @param string $name
	 * @return boolean
	 */
	public function sendEmailNewPass($name){
		$user = $this->userRepository->getUserByParams(array("name" => $name));
		if ($user == null){
			return false;
		}
		if ($user->getActive() == false){
			return false;
		}
		$token = $this->token();
		$user->setToken(md5($token["token"]));
		$user->setToken_validity($token["validity"]);
		$this->userRepository->save($user);

                $mail = new Mail\Message;
                $mail->setFrom(self::EMAIL_FROM)
                              ->addTo($user->getEmail())
                              ->setSubject('Žádost o nastavení nového hesla - Jubilee')
                              ->setHTMLBody('<p>Jubilee - Nastavení nového hesla<br />
						Uživatel: <strong>'.$name.'</strong><br />
						Nové heslo si můžete nastavit na následující stránce: <br /><br />
						<a href="http:/jubilee.jironett.cz/vstup/password/'.$name.'/'.$token["token"].'">http:/jubilee.jironett.cz/vstup/password/'.$name.'/'.$token["token"].'</a>
						<br /><br />Zrušení žádosti o nové heslo: <br />
						<a href="http:/jubilee.jironett.cz/vstup/cancel/'.$name.'/'.$token["token"].'">http:/jubilee.jironett.cz/vstup/cancel/'.$name.'/'.$token["token"].'</a>
						<br /> Platnost odkazů vyprší za 24 hodin.<br /><br />S pozdravem<br />Jironett.cz ;-)</p>');
                $mailer = new Mail\SendmailMailer;
		$mailer->send($mail);
		return true;
	}
	/**
	 * Returns key token and validity - unix timestamp
	 * @return array
	 */
	public function token(){
		$token = md5(uniqid(rand(), true));
		$validity = time()+60*60*24;
		return array("token" => $token, "validity" => $validity);
	}

	/**
	 * Verifies allow access based on token
	 * @param string $user
	 * @param int $token
	 * @param boolean $activation user = true, password|cancel = false
	 * @return boolean
	 */
	public function tokenVerify($name, $token, $activation = true, $cancel = false){
		if($name == null || $token == null){
			return false;
		}
		$user = $this->userRepository->getUserByParams(array("name" => $name));
		if($user == null){
			return false;
		}
		if($activation){
			if($user->getActive() == true){
				return false;
			}
		} elseif($user->getActive() == false){
				return false;
		}
		if($user->getToken() !== md5($token)){
			return false;
		}
		if($user->getToken_validity() < time()){
			$user->setToken(0);
			$this->userRepository->save($user);
			return false;
		}
		if ($cancel === true){
			$user->setToken(0);
		}
		$user->setActive(1);
		$this->userRepository->save($user);
		return true;
	}

	/**
	 * @param string $name
	 * @param string $password
	 * @param Jubilee\Model\Entities\User $user or null
	 * @return boolean - if user doesn't exist false | save pw true
	 */
	public function savePassword($name, $password, $user = null){
		if($user == null){
			$user = $this->userRepository->getUserByParams(array("name" => $name));
			if($user == null){
				return false;
			}
		}
		$user->setPassword($this->hashPW($password));
		$this->userRepository->save($user);
		return true;
	}

	/**
	 * @param string $name
	 * @param string $oldPassword
	 * @param string $password
	 * @return boolean - false if verify old password do not match
	 * @throws \Nette\UnexpectedValueException
	 */
	public function changePassword($name, $oldPassword, $password){
		$user = $this->userByName($name);
		if(!Security\Passwords::verify($oldPassword, $user->getPassword())){
			return false;
		}
		$this->savePassword($name, $password);
		return true;
	}

	/**
	 * @param string $name
	 * @param string $newEmail
	 * @param string $password
	 * @return boolean - if password is not correct returns false
	 * @throws \Nette\UnexpectedValueException
	 */
	public function changeEmail($name, $newEmail, $password){
		$user = $this->userByName($name);
		if(Security\Passwords::verify($password, $user->getPassword())){
			$user->setEmail($newEmail);
			$this->userRepository->save($user);
			return true;
		}
		return false;
	}

	/**
	 * @param string $email
	 * @return boolean
	 */
	public function emailExists($email){
		if ($this->userRepository->getUserByParams(array("email" => $email)) == NULL){
			return false;
		}
		return true;
	}

	/**
	 * @param string $password
	 * @return string  - hash password
	 */
	public function hashPW($password){
		return Security\Passwords::hash($password);
	}

	/**
	 * @param string $name
	 * @param string $password
	 * @return boolean - false if passwords are different
	 */
	public function deleteUser($name, $password){
		$user = $this->userByName($name);
		if(!Security\Passwords::verify($password, $user->getPassword())){
			return false;
		}
		$this->userRepository->delete($user);
		return true;
	}

	/**
	 * @param string $name
	 * @return \Jubilee\Model\Entities\User $user
	 * @throws \Nette\UnexpectedValueException
	 */
	public function userByName($name){
		$user = $this->userRepository->getUserByParams(array("name" => $name));
		if($user == null){
			throw new \Nette\UnexpectedValueException("user '".$name."' doesn't exist! ".__METHOD__);
		}
		return $user;
	}

	/**
	 * @param int
	 * @return \Jubilee\Model\Entities\User
	 */
	public function userByID($id){
		return $this->userRepository->getUserById($id);
	}
}