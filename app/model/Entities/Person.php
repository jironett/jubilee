<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */
namespace Jubilee\Model\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kappa\Doctrine\Entity\Entity;

/**
 * @ORM\Entity
 * @ORM\Table(name="person")
 */
class Person extends Entity
{
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $id_user;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $name;
	
	/**
	 * @ORM\Column(type="datetime")
	 */
	protected $birthday;	

	/**
	 * @ORM\Column(type="datetime")
	 */
	protected $feast;

	/**
	 * @ORM\Column(type="string") 
	 */	
	protected  $email;	
	/**
	 * @ORM\Column(type="string") 
	 */	
	protected $phone;
	
	 /**
	 * @ORM\Column(type="string") 
	 */
	protected  $note;	
	
}