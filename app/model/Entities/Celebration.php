<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */
namespace Jubilee\Model\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kappa\Doctrine\Entity\Entity;

/**
 * @ORM\Entity
 * @ORM\Table(name="celebration")
 */
class Celebration extends Entity
{
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $id_user;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $name;
	
	/**
	 * @ORM\Column(type="date")
	 */
	protected $date;	
	
	 /**
	 * @ORM\Column(type="string") 
	 */
	protected  $note;	
	
}