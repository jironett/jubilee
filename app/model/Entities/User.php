<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */
namespace Jubilee\Model\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kappa\Doctrine\Entity\Entity;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends Entity
{
	/**
	 * @ORM\Column(type="string")
	 */
	protected $name;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $password;
	
	/**
	 * @ORM\Column(type="string")
	 */
	protected $role;	

	/**
	 * @ORM\Column(type="string")
	 */
	protected $email;
	
	/**
	 * @ORM\Column(type="datetime") 
	 */
	protected $last_login;
	
	 /**
	 * @ORM\Column(type="datetime") 
	 */
	protected  $date_registration;
	
	/**
	 * @ORM\Column(type="boolean")
	 */
	protected $active;
	
	/**
	 * @ORM\Column(type="string")
	 */
	protected $token;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $token_validity;	
	
}