<?php

use Nette\Application\Routers\RouteList,
	Nette\Application\Routers\Route,
	Nette\Application\Routers\SimpleRouter;


/**
 * Router factory.
 */
class RouterFactory
{

	/**
	 * @return Nette\Application\IRouter
	 */
	public function createRouter()
	{
		$router = new RouteList();
		$router[] = $AppRouter = new RouteList();
		$AppRouter[] = new Route('registrace', 'Entrancepage:registration', RouteList::ONE_WAY);
		$AppRouter[] = new Route('zapomenute-heslo', 'Entrancepage:newPass', RouteList::ONE_WAY);
		$AppRouter[] = new Route('vstup/<action>[/<user>][/<token>]', array(
		    			'presenter' => array( Route::VALUE => "entrancepage",
					Route::FILTER_TABLE => array(
					'uzivatel' => 'entrancepage'
					)),
					'action' => array( Route::VALUE => 'default',
					Route::FILTER_TABLE => array(
					'registrace' => 'registration',
					'zapomenute-heslo' => 'newPass',
					'odhlasit' => 'out'
					))
				   ));
		$AppRouter[] = new Route('uzivatel/<action>[/<id>]', array(
		    			'presenter' => 'user',
					'action' => 'default'));		
		$AppRouter[] = new Route('<action>[/<id>]', array(
		    			'presenter' => 'jubilee',
					'action' => array( Route::VALUE => 'default',
					Route::FILTER_TABLE => array(
					'osoby' => 'person',
					'udalosti' => 'celebration'))
				   ));		
		return $router;
	}

}
