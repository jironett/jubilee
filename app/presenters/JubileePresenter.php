<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */

namespace Jubilee\Presenter;

use \Jironett\Utils\Arrays;

class JubileePresenter extends BasePresenter {
	
	/** @var \Jubilee\Model\Managers\PersonManager @inject */
	public $personManager;
	
	/** @var \Jubilee\Form\Jubilee\PersonForm @inject */
	public $personForm;
	
	/** @var \Jubilee\Model\Managers\CelebrationManager @inject */
	public $celebrationManager;
	
	/** @var \Jubilee\Form\Jubilee\CelebrationForm @inject */
	public $celebrationForm;	
	
	/** @var \Jubilee\Form\Jubilee\DeleteForm @inject */
	public $deleteForm;
	
	/** @var \Jubilee\Form\Jubilee\NumberDaysForm @inject */
	public $numberDaysForm;
	
	/** @var array */
	private $dataItems;
	
	/** @var int ID person or celebration */
	private $id;	
	
	public function renderDefault($numberDays = null){
		$celebrations = $this->celebrationManager->dashboardData($this->getUser()->id, $numberDays);
		$persons = $this->personManager->dashboardData($this->getUser()->id, $numberDays);		
		$this->template->data = Arrays::sortBySubArray(array_merge($persons, $celebrations), "days", false);
		$this->template->numPersons = $this->personManager->numPersons($this->getUser()->id);
		$this->template->numCelebrations = $this->celebrationManager->numCelebrations($this->getUser()->id);
	}
	
	public function renderPerson(){
		$this->template->persons = $this->dataItems;
	}

	public function renderCelebration(){
		$this->template->celebrations = $this->dataItems;
	}
	
	protected function createComponentAddNewPersonForm() {
		return $this->personForm->create();
	}
	
	protected function createComponentUpdatePersonForm() {
		return $this->personForm->create($this->id);
	} 	
	
	protected function createComponentAddNewCelebrationForm() {
		return $this->celebrationForm->create();
	} 
	
	protected function createComponentUpdateCelebrationForm() {
		return $this->celebrationForm->create($this->id);
	} 
	
	protected function createComponentDeleteForm() {
		return $this->deleteForm->create(Arrays::getValuesByItem($this->dataItems, "id"));
	}
	
	protected function createComponentNumberDays(){
		return $this->numberDaysForm->create();
	}
	
	public function actionPerson(){
		$this->dataItems = $this->personManager->getAllPersons($this->getUser()->id);		
	}
	
	public function actionCelebration(){
		$this->dataItems = $this->celebrationManager->getAllCelebrations($this->getUser()->id);	
	}
	
	public function actionUpdatePerson($id){
		if(!$this->personManager->verifyIdPerson($this->getUser()->id, $id)){
			$this->error("Page doesn't exist", '404');
		}
		$this->id = $id;
	}
	
	public function actionUpdateCelebration($id){
		if(!$this->celebrationManager->verifyIdCelebration($this->getUser()->id, $id)){
			$this->error("Page doesn't exist", '404');
		} 
		$this->id = $id;
	}
}
