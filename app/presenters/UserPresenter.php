<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */

namespace Jubilee\Presenter;


class UserPresenter extends BasePresenter {

	/** @var \Jubilee\Form\PasswordForm @inject */
	public $passwordForm;

	/** @var \Jubilee\Form\Jubilee\ChangeEmailForm @inject */
	public $changeEmailForm;

	/** @var \Jubilee\Form\Jubilee\DeleteUserForm @inject */
	public $deleteUserForm;

	/** @var \Jubilee\Model\Managers\UserManager @inject */
	public $userManager;

	protected function createComponentChangePasswordForm() {
		$changePwForm = $this->passwordForm->create($this->getUser()->identity->data["name"], false);
		$changePwForm->addPassword('oldPassword', 'Původní heslo: ')
			->setRequired('Zadejte své heslo!');
		return $changePwForm;
	}

	public function createComponentChangeEmailForm(){
		return $this->changeEmailForm->create();
	}

	public function createComponentDeleteUserForm(){
		return $this->deleteUserForm->create();
	}

	public function renderDefault(){
		$this->template->userData = $this->userManager->userByID($this->user->id);
	}

}
