<?php

namespace Jubilee\Presenter;

abstract class BasePresenter extends \Nette\Application\UI\Presenter
{
	public function startup()
	{
		parent::startup();
		if (!$this->getUser()->isLoggedIn()) {
			$this->redirect('Entrancepage:default');
		}
		if($this->presenter->name === "Jubilee"){
			switch($this->action){
				case "default" : $this->template->title = "Přehled"; break;
				case "person" : $this->template->title = "Správa osob"; break;
				case "celebration" : $this->template->title = "Správa událostí"; break;
			}
		}
	}

}