<?php
/**
 * This file is part of the Jubilee aplication.
 *
 * Copyright (c) 2014 Jiří Dušek (http://jironett.cz)
 *
 * For the full copyright and license information, please view the license.md
 * file that was distributed with this source code.
 */

namespace Jubilee\Presenter;

use \Nette\Application\UI\Presenter;

class EntrancepagePresenter extends Presenter
{
	/** @var \Jubilee\Form\Entrancepage\SignInForm @inject */
	public $signInForm;

	/** @var \Jubilee\Form\Entrancepage\RegistrationForm @inject */
	public $registrationForm;

	/** @var \Jubilee\Form\Entrancepage\NewPassForm @inject */
	public $newPassForm;

	/** @var \Jubilee\Form\PasswordForm @inject */
	public $passwordForm;

	/** @var \Jironett\Components\IJreCaptchaControlFactory @inject */
	public $jreCaptcha;

	/** @var \Jubilee\Model\Managers\UserManager @inject */
	public $userManager;

	function startup() {
		parent::startup();
		$this->setLayout('entrance');
	}

	function beforeRender() {
            parent::beforeRender();
	    if($this->action === "password"){
		if (!$this->userManager->tokenVerify($this->params['user'], $this->params['token'], false)){
			$this->redirect('Entrancepage:');
		}
	    }
        }

	protected function createComponentSignInForm() {
		return $this->signInForm->create();
	}

	protected function createComponentRegistrationForm() {
		return $this->registrationForm->create();
	}

	protected function createComponentNewPassForm() {
		return $this->newPassForm->create();
	}

	protected function createComponentPasswordForm() {
		return $this->passwordForm->create($this->params['user']);
	}

	protected function createComponentJreCaptcha(){
		return $this->jreCaptcha->create();
	}

	public function actionOut() {
		$this->getUser()->logout();
		$this->flashMessage('Byl jste odhlášen. Tak zas někdy ;-)');
		$this->redirect('Entrancepage:');
	}

	public function actionActivation() {
		if($this->userManager->tokenVerify($this->params["user"], $this->params["token"], true)){
			$this->flashMessage('Váš účet byl úspěšně aktivován ;-)', 'success');
			$this->redirect('Entrancepage:');
		}
		$this->redirect('Entrancepage:');
	}

	public function actionCancel() {
		if($this->userManager->tokenVerify($this->params["user"], $this->params["token"], false, true)){
			$this->flashMessage('Požadavek byl zrušen ;-)', 'success');
			$this->redirect('Entrancepage:');
		}
		$this->redirect('Entrancepage:');
	}

}